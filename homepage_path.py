caseregisterlink_path="//span[text()='Case Registration']"
verificationsupervision_path="//span[text()='Verification Supervision']"
dataentrysupervision_path="//span[text()='Data Entry Supervision']"
getrefno_path="//tr[@class='odd']/td[1]"
refnofilter_path="//div[@class='bootstrap-tagsinput']/input"
removerefno_path="//span[@data-role='remove']"
compmatchingnode_path="//*[@id='table']/tbody/tr/td/label/strong"
fromyear='2018'
frommonth='February'
frommonthn='02'
fromdate='25'
toyear='2018'
tomonth='March'
tomonthn='03'
todate='08'
lastsavedfrom_path='id=column4'
lastsavedfromyear_path="//strong[@class='ng-binding']"
lastsavedfromarrow_path="//button[@class='btn btn-default btn-sm pull-left']"
lastsavedfromyearselection_path="//span[text()='"+fromyear+"']"
lastsavedfrommonthselection_path="//span[text()='"+frommonth+"']"
lastsavedfromdateselection_path="//span[text()='"+fromdate+"']"
lastsavedto_path='id=column5'
lastsavedtoyear_path="//strong[@class='ng-binding']"
lastsavedtoarrow_path="//button[@class='btn btn-default btn-sm pull-left']"
lastsavedtoyearselection_path="//span[text()='"+toyear+"']"
lastsavedtomonthselection_path="//span[text()='"+tomonth+"']"
lastsavedtodateselection_path="//button/span[text()='"+todate+"']"
pagesize_path="//select/option[text()='100']"
rowsize_path="//tr[@class='odd']/td[4]"
vendorlink_path="//span[text()='Vendors']"
