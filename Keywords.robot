*** Settings ***
Variables         login.py

*** Keywords ***
Browser - Headless
    ${chrome_options}=    Evaluate    sys.modules['selenium.webdriver'].ChromeOptions()    sys, selenium.webdriver
    Call Method    ${chrome_options}    add_argument    headless
    Call Method    ${chrome_options}    add_argument    disable-gpu
    Create Webdriver    Chrome    chrome_options=${chrome_options}
    Set Window Size    1920    1080

Openworld Portal
    Go To    ${URL}
    input text    ${usernamepath}    ${ValidUsername}
    input password    ${passwordpath}    ${ValidPassword}
    Click button    ${loginbutton}

Openworld Portal Admin
    Go To    ${URL}
    input text    ${usernamepath}    ${admin_ValidUsername}
    input password    ${passwordpath}    ${admin_ValidPassword}
    Click button    ${loginbutton}

Openworld Portal - Client
    Go To    ${Client_URL}
    input text    ${usernamepath}    ${Client_ValidUsername}
    input password    ${passwordpath}    ${Client_ValidPassword}
    Click button    ${loginbutton}

Gmail
    Go To    ${GMAIL}

Service Provider Admin Login - portal
    Open Browser    ${URL}    ${Browser}
    Maximize Browser Window
    input text    ${usernamepath}    ${admin_ValidUsername}
    input password    ${passwordpath}    ${admin_ValidPassword}
    Click button    ${loginbutton}

Service Provider Login - portal
    Open Browser    ${URL}    ${Browser}
    Maximize Browser Window
    input text    ${usernamepath}    ${ValidUsername}
    input password    ${passwordpath}    ${ValidPassword}
    Click button    ${loginbutton}

Client Login - portal
    Open Browser    ${Client_URL}    ${Browser}
    Maximize Browser Window
    input text    ${usernamepath}    ${Client_ValidUsername}
    input password    ${passwordpath}    ${Client_ValidPassword}
    Click button    ${loginbutton}

Service Provider Login - mail
    Open Browser    ${GMAIL}    ${Browser}
    Maximize Browser Window

Random String
    ${randomfirstname}    Generate Random String    8    qwertyuioplkjhgfdsazxcvbnm
    ${randomlastname}    Generate Random String    5    qwertyuioplkjhgfdsazxcvbnm
    #Log    ${randomfirstname}
    Set Global Variable    ${randomfirstname}
    Set Global Variable    ${randomlastname}

Click Database
    ${xpath}=    Set Variable    ${checkbox_selection_path}
    Click Element    xpath=(${xpath})[${i}]
    Click Element    ${save_submit_path}
    Wait Until Element Is Visible    ${no_path}
    Element Text Should Be    ${message_path}    ${messagedb_Text_alert}
    Click Element    ${no_path}
    Page Should Contain    Database
    Sleep    3s
    Click Element    ${save_submit_path}
    Wait Until Element Is Visible    ${yes_path}
    Element Text Should Be    ${message_path}    ${messagedb_Text_alert}
    Click Element    ${yes_path}
    Wait Until Element Is Visible    ${ok_path}
    Element Text Should Be    ${message_path}    ${casesave_alert}
    Click Element    ${ok_path}
    Sleep    3s
    Wait Until Element Is Visible    ${verificationsupervision_path}
    Click Element    ${verificationsupervision_path}
    Sleep    3s
    Wait Until Element Is Visible    ${clientfilter_path}
    Input Text    ${clientfilter_path}    ${clientname}
    Press Key    ${clientfilter_path}    \\13
    Input Text    ${firstnamefilter_path}    ${randomfirstname}
    Input Text    ${lastnamefilter_path}    ${randomlastname}
    Click Element    ${filtersearch_path}
    Clear Element Text    ${firstnamefilter_path}
    Clear Element Text    ${lastnamefilter_path}
    Click Element    ${clientfilterclose_path}
    ${firstletter}=    Get substring    ${randomfirstname}    0    1
    ${upperletter}=    Convert To Uppercase    ${firstletter}
    ${nextletters}=    Get substring    ${randomfirstname}    1
    ${randomfirstname}=    Catenate    ${upperletter}${nextletters}
    ${firstletter}=    Get substring    ${randomlastname}    0    1
    ${upperletter}=    Convert To Uppercase    ${firstletter}
    ${nextletters}=    Get substring    ${randomlastname}    1
    ${randomlastname}=    Catenate    ${upperletter}${nextletters}
    Page Should Contain    ${randomfirstname}
    Page Should Contain    ${randomlastname}
    Page Should Contain    ${clientname}
    Log To Console    Case Available in Verification Stage
    Close Browser

Select Checkbox
    ${xpath}=    Set Variable    ${checkbox_selection_path}
    Click Element    xpath=(${xpath})[${i}]

Select Checkbox Count
    ${xpath}=    Set Variable    ${checkbox_selection_path}
    Click Element    xpath=(${xpath})[${i}]
    ${value_count}=    Evaluate    ${value_count}+1
    Set Global Variable    ${value_count}

Checkbox Checked
    ${xpath}=    Set Variable    ${checkbox_selection_path}
    Checkbox Should Be Selected    xpath=(${xpath})[${i}]

Checkbox Unchecked
    ${xpath}=    Set Variable    ${checkbox_selection_path}
    Checkbox Should Not Be Selected    xpath=(${xpath})[${i}]

Select Checkbox Database
    ${xpath}=    Set Variable    ${checkbox_selection_path}
    ${status}=    Run Keyword And Return Status    Checkbox Should be Selected    xpath=(${xpath})[${i}]
    Run Keyword if    '${status}' != 'True'    Select Checkbox
    Click Element    ${databasecomponent_path}
    ${status}=    Run Keyword And Return Status    Checkbox Should be Selected    ${databaseselectall_path}
    Run Keyword if    '${status}' == 'True'    Fail
    Click Element    ${save_path}
    Wait Until Element Is Visible    ${ok_path}
    Click Element    ${ok_path}
    Input Text    ${firstnamefilter_path}    ${randomfirstname}
    Input Text    ${lastnamefilter_path}    ${randomlastname}
    Wait Until Element Is Visible    ${clientfilter_path}
    Input Text    ${clientfilter_path}    ${clientname}
    Press Key    ${clientfilter_path}    \\13
    Click Element    ${filtersearch_path}
    Sleep    3s
    Wait Until Element Is Visible    ${click_case_path}
    Click Element    ${click_case_path}
    ${status}=    Run Keyword And Return Status    Checkbox Should be Selected    ${databaseselectall_path}
    Run Keyword if    '${status}' == 'True'    Fail
    ${status}=    Run Keyword And Return Status    Checkbox Should be Selected    ${databasecomponent_path}
    Run Keyword if    '${status}' == 'True'    Fail
    Click Element    ${databaseselectall_path}
    Click Element    ${save_path}
    Sleep    2s
    Wait Until Element Is Visible    ${ok_path}
    Click Element    ${ok_path}
    Input Text    ${firstnamefilter_path}    ${randomfirstname}
    Input Text    ${lastnamefilter_path}    ${randomlastname}
    Wait Until Element Is Visible    ${clientfilter_path}
    Input Text    ${clientfilter_path}    ${clientname}
    Press Key    ${clientfilter_path}    \\13
    Click Element    ${filtersearch_path}
    Sleep    2s
    Wait Until Element Is Visible    ${click_case_path}
    Click Element    ${click_case_path}
    ${status}=    Run Keyword And Return Status    Checkbox Should be Selected    ${databaseselectall_path}
    Run Keyword if    '${status}' == 'False'    Fail
    Click Element    ${save_path}
    Sleep    2s
    Wait Until Element Is Visible    ${ok_path}
    Click Element    ${ok_path}

Uncheck Select All
    Checkbox Should not be Selected    ${selectallcomponent_path}

Check Select All
    Checkbox Should be Selected    ${selectallcomponent_path}
