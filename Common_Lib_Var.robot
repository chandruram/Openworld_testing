*** Settings ***
Variables         case_registration_inputs.py
Library           DateTime
Library           Collections
Variables         case_registration_path.py
Variables         candidate_page.py
Variables         create_password.py
Variables         gmail_path.py
Variables         homepage_path.py
Variables         ../../Downloads/text.py
Variables         dataentry_path.py
Library           String
Library           ExcelLibrary
Library           Selenium2Library
Variables         vendor_master_path.py
Resource          Keywords.robot
Variables         vendor_inputs.py
Variables         sitepreference_path.py

*** Variables ***
${value_count}    0
@{multiple_component}    ${component_eduselect}    ${component_refselect}    ${component_addselect}
@{multiple_selected_check}    ${vendorcheck1}    ${vendorcheck3}    ${vendorcheck5}    ${vendorcheck6}
@{multiple_deselected_check}    ${vendorcheck2}    ${vendorcheck4}
#path - Case Registration
${fatherfname_mandatorystar}    //label[contains(text(),"Father's First Name")]/em
${fatherlname_mandatorystar}    //label[contains(text(),"Father's Last Name")]/em
